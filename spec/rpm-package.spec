specName:       rpm-package
Version:        1.0
Release:        1%{?dist}
Summary:        Это пример минимального RPM пакета

License:        MIT
URL:            http://example.com/project
Source0:        http://example.com/project/rpm-package-1.0.tar.gz

BuildRequires:  make, gcc
Requires:       libc

%description
Этот пакет представляет собой минимальный пример, включая основные элементы spec файла и функцию логирования.

%prep
%setup -q

%build
make %{?_smp_mflags}

%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT

%check
make test

%files
%defattr(-,root,root,-)
/usr/bin/mypackage

%post
echo "Установка завершена." >> /var/log/mypackage-install.log

%preun
if [ $1 -eq 0 ]; then # При удалении пакета
    echo "Пакет mypackage будет удален." >> /var/log/mypackage-install.log
fi

%postun
if [ $1 -eq 0 ]; then # После удаления пакета
    echo "Пакет mypackage успешно удален." >> /var/log/mypackage-install.log
fi

