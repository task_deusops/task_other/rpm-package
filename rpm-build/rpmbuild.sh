#!/bin/bash

# Проверка входных параметров
if [ "$#" -ne 2 ]; then
    echo "Usage: $0 <image_tag> <dockerfile_path>"
    exit 1
fi

IMAGE_TAG=$1
DOCKERFILE_PATH=$2
IMAGE_NAME="avetis74/rpmbuild"

# build образа
docker build -t $IMAGE_NAME -f $DOCKERFILE_PATH .

# Тегирование образа
docker tag $IMAGE_NAME "$IMAGE_NAME:$IMAGE_TAG"

# Пуш образа в Docker Hub (или другой регистри)
docker push "$IMAGE_NAME:$IMAGE_TAG"

echo "Image '$IMAGE_TAG' has been built and pushed to the registry."

