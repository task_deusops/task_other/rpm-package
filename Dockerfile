FROM avetis74/rpmbuild:v0.9

WORKDIR /src
ARG buildNumber
COPY . .
RUN chmod +x rpm_build.sh
RUN ./rpm_build.sh ${buildNumber} /src
